import sys
import time
import traceback

import paho.mqtt.client as mqtt
from RPi import GPIO

from pyparts.platforms import raspberrypi_platform as rpi_platform

UP_LIMIT_SWITCH = 8
DOWN_LIMIT_SWITCH = 10

STEPPER_ENABLE = 40
STEPPER_STEP = 12
STEPPER_DIRECTION = 18

BUTTON = 7

PIN_NAMING_SCHEME = rpi_platform.BOARD

POSITION_UP = 'up'
POSITION_DOWN = 'down'
POSITION = POSITION_UP
NEEDS_POSITION_CHANGE = False

_MQTT_USERNAME = 'sean'
_MQTT_PASSWORD = '<redacted>'
_MQTT_HOST = 'mosquitto.seanwatson.io'
_MQTT_CLIENT_ID = 'tablemotor'
_MQTT_TLS_CA_FILE = '/home/pi/certs.pem'
_MQTT_TOPIC_ROOT = 'homeautomation/switches/tablemotor/'


def change_postion():
  global NEEDS_POSITION_CHANGE
  NEEDS_POSITION_CHANGE = True


def on_connect(client, userdata, flags, rc):
  """Callback for when MQTT connects to the server.

  Args:
    client: MQTT client object.
    userdata: Private userdata set at client creation time.
    flags: Response flags.
    rc: Connection result.
  """
  client.subscribe(_MQTT_TOPIC_ROOT + '#')


def on_message(client, userdata, msg):
  """Callback for when MQTT receives a message.

  Args:
    client: MQTT client object.
    userdata: Private userdata set at client creation time.
    msg: The topic and payload that was published.
  """
  global NEEDS_POSITION_CHANGE
  global POSITION_UP
  global POSITION_DOWN
  global POSITION
  try:
    if msg.topic == _MQTT_TOPIC_ROOT + 'set':
      if ((msg.payload == 'ON' and POSITION == POSITION_DOWN) or
          (msg.payload == 'OFF' and POSITION == POSITION_UP)):
        NEEDS_POSITION_CHANGE = True
  except Exception as e:
    pass


def main():
  global NEEDS_POSITION_CHANGE
  global POSITION_UP
  global POSITION_DOWN
  global POSITION
  platform = rpi_platform.RaspberryPiPlatform(PIN_NAMING_SCHEME)

  stepper_enable = platform.get_digital_output(STEPPER_ENABLE)
  stepper_enable.set_low()

  stepper_direction = platform.get_digital_output(STEPPER_DIRECTION)
  stepper_direction.set_low()

  stepper_step = platform.get_pwm_output(STEPPER_STEP)
  stepper_step.frequency_hz = 5000
  stepper_step.disable()

  up_limit_switch_input = platform.get_digital_input(UP_LIMIT_SWITCH)
  down_limit_switch_input = platform.get_digital_input(DOWN_LIMIT_SWITCH)

  button_input = platform.get_digital_input(BUTTON)
  button_ready = False

  client = mqtt.Client(client_id=_MQTT_CLIENT_ID)
  client.tls_set(_MQTT_TLS_CA_FILE)
  client.tls_insecure_set(False)
  client.username_pw_set(_MQTT_USERNAME, _MQTT_PASSWORD)
  client.on_connect = on_connect
  client.on_message = on_message
  client.connect(_MQTT_HOST)
  client.loop_start()

  while True:
    try:
      up_limit_switch_is_pressed = up_limit_switch_input.is_low()
      down_limit_switch_is_pressed = down_limit_switch_input.is_low()
      
      if up_limit_switch_is_pressed == down_limit_switch_is_pressed:
        time.sleep(5)
        continue

      if up_limit_switch_is_pressed:
        POSITION = POSITION_UP
      else:
        POSITION = POSITION_DOWN

      if button_input.is_high() and not button_ready:
        time.sleep(0.05)
        if button_input.is_high():
          button_ready = True

      if button_input.is_low() and button_ready:
        time.sleep(0.05)
        if button_input.is_low():
          button_ready = False
          NEEDS_POSITION_CHANGE = True

      if NEEDS_POSITION_CHANGE:
        if up_limit_switch_is_pressed:
          stepper_enable.set_high()
          stepper_direction.set_high()
          stepper_step.enable()
          stepper_step.duty_cycle = 50
          stepper_step.frequency_hz = 5000
          while True:
            if down_limit_switch_input.is_low():
              time.sleep(0.05)
              if down_limit_switch_input.is_low():
                break
            time.sleep(0.01)
          stepper_enable.set_low()
          stepper_step.disable()
          stepper_direction.set_low()
          client.publish(_MQTT_TOPIC_ROOT + 'status',
                         payload='OFF',
                         qos=2,
                         retain=True)
        elif down_limit_switch_is_pressed:
          stepper_enable.set_high()
          stepper_direction.set_low()
          stepper_step.enable()
          stepper_step.duty_cycle = 50
          stepper_step.frequency_hz = 5000
          while True:
            if up_limit_switch_input.is_low():
              time.sleep(0.05)
              if up_limit_switch_input.is_low():
                break
            time.sleep(0.01)
          stepper_enable.set_low()
          stepper_direction.set_high()
          stepper_step.disable()
          client.publish(_MQTT_TOPIC_ROOT + 'status',
                         payload='ON',
                         qos=2,
                         retain=True)
        NEEDS_POSITION_CHANGE = False

      time.sleep(1)
    except (Exception, KeyboardInterrupt) as e:
      traceback.print_exc(file=sys.stdout)
      stepper_enable.set_low()
      stepper_direction.set_high()
      stepper_step.disable()
      GPIO.cleanup()
      client.loop_stop()
      client.disconnect()
      sys.exit()


if __name__ == '__main__':
    main()
